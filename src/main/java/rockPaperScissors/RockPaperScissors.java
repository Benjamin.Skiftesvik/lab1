package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    boolean x = true;
    
    public void run() {
       
        while (x == true){
        System.out.println("Let's play round " + roundCounter);
        String input = readInput("Your choice (Rock/Paper/Scissors)?");

        if (input.equals("rock")){
            if (random_computer_choice()=="paper"){
                System.out.println("Human chose rock, computer chose paper. Computer wins!");
                computerScore ++ ;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            }
            else if(random_computer_choice() == "rock"){
                System.out.println("Human chose rock, computer chose rock. it's at tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else{
                System.out.println("Human chose rock, computer chose scissors. Human wins!");
                humanScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
        }
        if (input.equals("paper")){
            if (random_computer_choice()=="scissors"){
                System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                computerScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else if(random_computer_choice() == "paper"){
                System.out.println("Human chose paper, computer chose paper. it's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else{
                System.out.println("Human chose paper, computer chose rock. Human wins!");
                humanScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
        }
    
        if (input.equals("scissors")){
            if (random_computer_choice()=="rock"){
                System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                computerScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else if(random_computer_choice() == "scissors"){
                System.out.println("Human chose scissors, computer chose scissors. it's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else{
                System.out.println("Human chose scissors, computer chose paper. Human wins!");
                humanScore++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
        }


        String continue1 = readInput("Do you wish to continue playing? (y/n)?");
        if (continue1.equals("n")){
            System.out.println("Bye bye :)");
            break;
        }
        roundCounter++;


        }
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }


    public static String random_computer_choice() {
    
        Random rand = new Random();
        String[] computer_choices = {"rock","paper","scissors"};
    
        String computer_choice = computer_choices[rand.nextInt(3)];
        return computer_choice;
}

}
